package views;

import Controller.ThreadCalc;

public class Principal {

	/*
	 * Aula de Threads e Processos
	 * author: Erisvaldo Correia
	 * Sistemas Operacionais
	 */
	public static void main(String[] args){
		int a = 10;
		int b = 2;
		//int op = 2;
		
		/*
		 * Percorrendo a Thread de 0 at� 3 na opera��o
		 */
		for (int op = 0; op <= 3; op++) {
			Thread tCalc = new ThreadCalc(a, b, op);
			tCalc.start();
		}
 	}
}
